package adminiot.arleide.com.adminiot.adapter;

import android.app.ListActivity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import adminiot.arleide.com.adminiot.R;

import adminiot.arleide.com.adminiot.model.Room;

/**
 * Created by root on 11/8/16.
 */

public class RoomAdapter extends BaseAdapter {

    private List<Room> rooms;
    private LayoutInflater inflater;

    public RoomAdapter(List<Room> rooms, Context context) {
        this.rooms = rooms;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {

        return rooms.size();
    }

    @Override
    public Object getItem(int i) {

        return rooms.get(i);
    }

    @Override
    public long getItemId(int i) {

        return rooms.get(i).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View itemlayout_room = inflater.inflate(R.layout.itemlayout_room, parent, false);
        Room room = rooms.get(position);

        TextView roomName = (TextView) itemlayout_room.findViewById(R.id.itemNameRoom);
        RelativeLayout layout_roomName = (RelativeLayout) itemlayout_room.findViewById(R.id.item_layoutRoom_id);

        if (position % 2 == 0) {
            // layout_placeName.setBackgroundColor(Color.BLUE);
            layout_roomName.setBackgroundResource(R.color.colorListviewLinhaClara);
        } else {
            layout_roomName.setBackgroundResource(R.color.colorListviewLinhaEscura);
        }

        roomName.setText(room.getName());


        return itemlayout_room;
    }

    public void updateListRooms(List<Room> roomList) {
        this.rooms = roomList;
        notifyDataSetChanged();
    }
}
